export APP_CONF=config/local.yml

mysql -h 127.0.0.1 -uroot -p123456 -P 3380 user


curl -X 'GET' \
  'http://localhost:8000/v1/user' \
  -H 'accept: application/json' \
  -H 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiJ6VGdRU2NCYnJsIiwiZXhwIjoxNzA2OTA0NTQ4LCJuYmYiOjE2OTkxMjg1NDgsImlhdCI6MTY5OTEyODU0OH0.9KUfSamIGy6FI9Qj_fXv7dQ3ZrPPEmYHkVnfA8ulnJ8'


curl -X 'POST' \
  'http://localhost:8000/v1/order' \
  -H 'accept: application/json' \
  -d '{"title":"New Book"}'

curl -X 'GET' \
  'http://localhost:8000/v1/orders' \
  -H 'accept: application/json'

curl -X 'GET' \
  'http://localhost:8000/v1/user' \
  -H 'accept: application/json'

curl -X 'POST' \
  'https://advancedapi.demoproject.one/v1/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "password": "123456",
  "username": "alan"
}'

curl -X 'GET' \
  'https://advancedapi.demoproject.one/v1/orders' \
  -H 'accept: application/json'
 



nunu wire all
APP_CONF=config/local.yml go run ./cmd/migration

docker run --rm -i 1.1.1.1:5000/demo-job:v1

docker run --rm -i remotejob/advanced-api:v0.0.0


CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) NOT NULL,
  `username` varchar(191) NOT NULL,
  `nickname` longtext NOT NULL,
  `password` longtext NOT NULL,
  `email` longtext NOT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `idx_users_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(191) NOT NULL,
  `title` longtext NOT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`),
  KEY `idx_orders_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

kubectl config set-context --current --namespace advanced-api

k apply -f deploy/k8s/advancedapiservice.yaml
k delete -f deploy/k8s/advancedapiservice.yaml

kubectl port-forward svc/advancedapi-svc 8000