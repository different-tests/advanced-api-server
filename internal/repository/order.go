package repository

import (
	"advanced-api-server/internal/model"
	"context"
	"log"
)

type OrderRepository interface {
	Create(ctx context.Context, order *model.Order) error
	FirstById(id int64) (*model.Order, error)
	GetAllOrders() ([]*model.Order, error)
}

func NewOrderRepository(repository *Repository) OrderRepository {
	return &orderRepository{
		Repository: repository,
	}
}

type orderRepository struct {
	*Repository
}

func (r *orderRepository) Create(ctx context.Context, order *model.Order) error {
	if err := r.db.Create(order).Error; err != nil {
		return err
	}
	return nil
}

func (r *orderRepository) GetAllOrders() ([]*model.Order, error) {

	var result []*model.Order

	var orders []model.Order

	if err := r.db.Find(&orders).Error; err != nil {
		return nil, err
	}

	for _, res := range orders {

		log.Println(res)

		ord := new(model.Order)

		ord.OrderId = res.OrderId
		ord.Title = res.Title
		ord.CreatedAt = res.CreatedAt
		ord.UpdatedAt = res.UpdatedAt

		result = append(result, ord)

	}

	return result, nil
}

func (r *orderRepository) FirstById(id int64) (*model.Order, error) {
	var order model.Order
	// TODO: query db
	return &order, nil
}
