package service

import (
	v1 "advanced-api-server/api/v1"
	"advanced-api-server/internal/model"
	"advanced-api-server/internal/repository"
	"context"
)

type OrderService interface {
	GetOrderById(id int64) (*model.Order, error)
	CreateOrder(ctx context.Context, req *v1.RequestCreateOrder) error
	GetAllOrders(ctx context.Context) ([]*model.Order, error)
}

func NewOrderService(service *Service, orderRepository repository.OrderRepository) OrderService {
	return &orderService{
		Service:         service,
		orderRepository: orderRepository,
	}
}

type orderService struct {
	*Service
	orderRepository repository.OrderRepository
}

func (s *orderService) GetAllOrders(ctx context.Context) ([]*model.Order, error) {
	return s.orderRepository.GetAllOrders()

}

func (s *orderService) CreateOrder(ctx context.Context, req *v1.RequestCreateOrder) error {

	// Create a Order
	order := &model.Order{
		OrderId: req.OrderID,
		Title:   req.Title,
	}
	if err := s.orderRepository.Create(ctx, order); err != nil {
		return err
	}

	return nil
}

func (s *orderService) GetOrderById(id int64) (*model.Order, error) {
	return s.orderRepository.FirstById(id)
}
