package handler

import (
	v1 "advanced-api-server/api/v1"
	"advanced-api-server/internal/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type OrderHandler interface {
	GetOrder(ctx *gin.Context)
	CreateOrder(ctx *gin.Context)
	GetAllOrder(ctx *gin.Context)
}

func NewOrderHandler(handler *Handler, orderService service.OrderService) OrderHandler {
	return &orderHandler{
		Handler:      handler,
		orderService: orderService,
	}
}

type orderHandler struct {
	*Handler
	orderService service.OrderService
}

func (h *orderHandler) GetOrder(ctx *gin.Context) {

}

// Register godoc
// @Summary Get All Ordrs
// @Schemes
// @Description Get All Ordrs
// @Tags Orders
// @Accept json
// @Produce json
// @Success 200 {object} v1.Response
// @Router /orders [get]
func (h *orderHandler) GetAllOrder(ctx *gin.Context) {

	orders, err := h.orderService.GetAllOrders(ctx)
	if err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}
	v1.HandleSuccess(ctx, orders)

}

// Register godoc
// @Summary Order
// @Schemes
// @Description Create New Order
// @Tags Orders
// @Accept json
// @Produce json
// @Security Bearer
// @Param request body v1.RequestCreateOrder true "params"
// @Success 200 {object} v1.Response
// @Router /order [post]
func (h *orderHandler) CreateOrder(ctx *gin.Context) {

	userId := GetUserIdFromCtx(ctx)
	if userId == "" {
		v1.HandleError(ctx, http.StatusUnauthorized, v1.ErrUnauthorized, nil)
		return
	}

	req := new(v1.RequestCreateOrder)
	if err := ctx.ShouldBindJSON(req); err != nil {
		v1.HandleError(ctx, http.StatusBadRequest, v1.ErrBadRequest, nil)
		return
	}

	if err := h.orderService.CreateOrder(ctx, req); err != nil {
		h.logger.WithContext(ctx).Error("userService.Register error", zap.Error(err))
		v1.HandleError(ctx, http.StatusInternalServerError, err, nil)
		return
	}

	v1.HandleSuccess(ctx, nil)
}
