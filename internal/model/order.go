package model

import (
	"time"

	"gorm.io/gorm"
)

type Order struct {
	Id        uint   `gorm:"primarykey"`
	OrderId   string `gorm:"unique;not null"`
	Title     string `gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type Orders []Order

func (o *Order) TableName() string {
	return "orders"
}
