//go:build wireinject
// +build wireinject

package wire

import (
	"advanced-api-server/internal/handler"
	"advanced-api-server/internal/repository"
	"advanced-api-server/internal/server"
	"advanced-api-server/internal/service"
	"advanced-api-server/pkg/app"
	"advanced-api-server/pkg/helper/sid"
	"advanced-api-server/pkg/jwt"
	"advanced-api-server/pkg/log"
	"advanced-api-server/pkg/server/http"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var handlerSet = wire.NewSet(
	handler.NewHandler,
	handler.NewUserHandler,
	handler.NewOrderHandler,
)

var serviceSet = wire.NewSet(
	service.NewService,
	service.NewUserService,
	service.NewOrderService,
)

var repositorySet = wire.NewSet(
	repository.NewDB,
	// repository.NewRedis,
	repository.NewRepository,
	repository.NewUserRepository,
	repository.NewOrderRepository,
)
var serverSet = wire.NewSet(
	server.NewHTTPServer,
	server.NewJob,
	server.NewTask,
)

// build App
func newApp(httpServer *http.Server, job *server.Job) *app.App {
	return app.NewApp(
		app.WithServer(httpServer, job),
		app.WithName("demo-server"),
	)
}

func NewWire(*viper.Viper, *log.Logger) (*app.App, func(), error) {

	panic(wire.Build(
		repositorySet,
		serviceSet,
		handlerSet,
		serverSet,
		sid.NewSid,
		jwt.NewJwt,
		newApp,
	))
}
