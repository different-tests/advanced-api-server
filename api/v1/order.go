package v1

type RequestCreateOrder struct { 
	OrderID string `json:"order_id" binding:"required" example:"xxxxxxxxx01"`
	Title string `json:"title" binding:"required" example:"New Book"`

}